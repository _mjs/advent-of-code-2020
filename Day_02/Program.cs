﻿using System;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace Day_02
{
    class Program
    {
        static void Main(string[] args)
        {
            var passwords = File.ReadAllLines("input.txt").AsEnumerable()
                .Select(p => new Password(p));

            Console.WriteLine($"Part 1: {passwords.Count(p => p.Part1_IsValid())} valid passwords");
            Console.WriteLine($"Part 2: {passwords.Count(p => p.Part2_IsValid())} valid passwords");
        }
    }

    class Password
    {
        public Password(string input)
        {
            var patternMatch = new Regex("(\\d+)-(\\d+)\\s(.):\\s(.+)");
            var match = patternMatch.Match(input);

            Low = int.Parse(match.Groups[1].Value);
            High = int.Parse(match.Groups[2].Value);
            Char = Convert.ToChar(match.Groups[3].Value);
            Value = match.Groups[4].Value;
        }

        public int Low { get; set; }
        public int High { get; set; }
        public char Char { get; set; }
        public string Value { get; set; }

        public bool Part1_IsValid()
        {
            var count = Value.Count(v => v == Char);
            return count >= Low && count <= High;
        }

        public bool Part2_IsValid() => (Value[Low - 1] == Char) ^ (Value[High - 1] == Char);
    }
}
