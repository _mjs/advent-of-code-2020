﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace Day_04
{
    class Program
    {
        static void Main(string[] args)
        {
            var passports = File.ReadAllText("input.txt")
                .Split(Environment.NewLine + Environment.NewLine)
                .Select(p => new Passport(p));

            Console.WriteLine($"Part 1: {passports.Count(p => p.IsValid_Part1)} valid passports");
            Console.WriteLine($"Part 2: {passports.Count(p => p.IsValid_Part2)} valid passports");
        }
    }

    class Passport
    {
        private readonly Dictionary<string, string> _fields = new Dictionary<string, string>();
        private readonly IList<string> _requiredFields = new[] { "byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid" };

        private bool _hasInvalidFieldValue = false;
        private string p;

        public Passport(string value)
        {
            value.Replace(Environment.NewLine, " ")
                .Split(' ')
                .ToList()
                .ForEach(field => ValidateAndAdd(field));
        }

        private void ValidateAndAdd(string field)
        {
            var key = field.Split(':')[0];
            var value = field.Split(':')[1];

            if (!Validator.ValidateValue[key](value))
                _hasInvalidFieldValue = true;

            _fields.Add(key, value);
        }

        public bool IsValid_Part1 => _requiredFields.All(f => _fields.ContainsKey(f));
        public bool IsValid_Part2 => !_hasInvalidFieldValue && IsValid_Part1;
    }

    class Validator
    {
        public static Dictionary<string, Func<string, bool>> ValidateValue
            = new Dictionary<string, Func<string, bool>>()
        {
            { "byr", ByrValid },
            { "iyr", IyrValid },
            { "eyr", EyrValid },
            { "hgt", HgtValid },
            { "hcl", HclValid },
            { "ecl", EclValid },
            { "pid", PidValid },
            { "cid", CidValid }
        };

        static bool CidValid(string value) => true;

        static bool ByrValid(string value)
        {
            var byr = int.Parse(value);
            return byr >= 1920 && byr <= 2002;
        }

        static bool IyrValid(string value)
        {
            var isy = int.Parse(value);
            return isy >= 2010 && isy <= 2020;
        }

        static bool EyrValid(string value)
        {
            var eyr = int.Parse(value);
            return eyr >= 2020 && eyr <= 2030;
        }

        static bool HgtValid(string value)
        {
            var pattern = new Regex("(\\d+)(cm|in)");
            if (!pattern.IsMatch(value)) return false;

            var v = int.Parse(pattern.Match(value).Groups[1].Value);
            var m = pattern.Match(value).Groups[2].Value;

            if (m == "cm") return v >= 150 && v <= 193;
            return v >= 59 && v <= 76;
        }

        static bool HclValid(string value) => new Regex("(#[0-9a-f]{6})").IsMatch(value);

        static bool EclValid(string value) => new[] { "amb", "blu", "brn", "gry", "grn", "hzl", "oth" }.Contains(value);

        static bool PidValid(string value) => int.TryParse(value, out var _) && value.Length == 9;
    }
}
