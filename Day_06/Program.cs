﻿using System;
using System.IO;
using System.Linq;

namespace Day_06
{
    class Program
    {
        static void Main(string[] args)
        {
            var groups = File.ReadAllText("input.txt")
                .Split(Environment.NewLine + Environment.NewLine)
                .Select(g => new Group(g));

            Console.WriteLine($"Part 1: {groups.Sum(g => g.AnyoneAnsweredYes)}");
            Console.WriteLine($"Part 2: {groups.Sum(g => g.EveryoneAnsweredYes)}");
        }
    }

    class Group
    {
        public Group(string answers)
        {
            AnyoneAnsweredYes = answers.Replace(Environment.NewLine, "").Distinct().Count();

            var gSize = answers.Replace(Environment.NewLine, " ").Split(' ').Count();
            Enumerable.Range(0, 26).Select(i => Convert.ToChar('a' + i))
                .ToList()
                .ForEach(c =>
                {
                    if (answers.Count(a => a == c) == gSize)
                        EveryoneAnsweredYes++;
                });
        }

        public int AnyoneAnsweredYes { get; } = 0;
        public int EveryoneAnsweredYes { get; set; } = 0;
    }
}
