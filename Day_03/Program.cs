﻿using System;
using System.Drawing;
using System.IO;
using System.Linq;

namespace Day_03
{
    class Program
    {
        static void Main(string[] args)
        {
            var map = File.ReadAllLines("input.txt").ToArray();

            Console.WriteLine($"Part 1: {TraverseSlope(map, 3, 1)}");

            Console.WriteLine($@"Part 2: {
                TraverseSlope(map, 1, 1) *
                TraverseSlope(map, 3, 1) *
                TraverseSlope(map, 5, 1) *
                TraverseSlope(map, 7, 1) *
                TraverseSlope(map, 1, 2)
            }");
        }

        private static ulong TraverseSlope(string[] map, int right, int down)
        {
            var rightEdge = map[0].Length;
            var bottomEdge = map.Count();

            var position = new Point(0, 0);
            ulong trees = 0;

            while (position.Y < bottomEdge - 1)
            {
                position.X = (position.X + right) % rightEdge;
                position.Y += down;

                trees += map[position.Y][position.X] == '#' ? 1u : 0;
            }

            return trees;
        }
    }
}
