﻿using System;
using System.IO;
using System.Linq;

namespace Day_01
{
    class Program
    {
        static void Main(string[] args)
        {
            const int TARGET = 2020;

            var input = File.ReadAllLines("input.txt").AsEnumerable()
                .Select(int.Parse)
                .OrderBy(e => e);

            foreach (var entry in input)
            {
                var match = TARGET - entry;

                var matchExists = input.Any(e => e == match);

                if (matchExists)
                {
                    Console.WriteLine($"Part 1: {entry * match}");
                    break;
                }
            }

            var j = 0;
            var k = 0;

            for(var i = 0; i < input.Count() - 1; i++)
            {
                j = i + 1;
                k = input.Count() - 1;

                while(k >= j)
                {
                    var one = input.ElementAt(i);
                    var two = input.ElementAt(j);
                    var three = input.ElementAt(k);
                    
                    if (one + two + three == TARGET)
                    {
                        Console.WriteLine($"Part 2: {one * two * three}");
                        break;
                    } 
                    else if (one + two + three > TARGET)
                    {
                        k--;
                    }
                    else
                    {
                        j++;
                    }
                }
            }
        }
    }
}
