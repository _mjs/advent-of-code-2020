﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Day_05
{
    class Program
    {
        static void Main(string[] args)
        {
            var boardingPasses = File.ReadAllLines("input.txt");
            var seatIds = new List<int>();

            foreach (var pass in boardingPasses)
            {
                seatIds.Add(
                    ReduceArray(pass.Take(7), Enumerable.Range(0, 128), 'F') * 8
                    + ReduceArray(pass.Skip(7), Enumerable.Range(0, 8), 'L'));
            };

            Console.WriteLine($"Part 1: {seatIds.Max()}");

            var mySeat = Enumerable.Range(seatIds.First(), seatIds.Last())
                .Except(seatIds)
                .First();

            Console.WriteLine($"Part 2: {mySeat}");
        }

        static int ReduceArray(IEnumerable<char> pass, IEnumerable<int> seats, char check)
        {
            foreach (var instruction in pass)
            {
                seats = instruction == check
                    ? seats.Take(seats.Count() / 2).ToArray()
                    : seats.Skip(seats.Count() / 2).ToArray();
            }

            return seats.First();
        }
    }
}
